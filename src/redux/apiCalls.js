import { loginStart, loginSuccess, loginFailure, registerStart, registerSuccess, registerFailed, loggingOut  } from "./userRedux";
import { publicRequest } from "../requestMethods";
import { Redirect } from "react-router-dom"


export const login = async (dispatch, user) => {
  dispatch(loginStart());
  try {
    const res = await publicRequest.post("/auth/login", user);
    dispatch(loginSuccess(res.data));
  } catch (err) {
    dispatch(loginFailure());
  }
};

export const register = async (dispatch, user) => {

  dispatch(registerStart());
  try {
  const res = await publicRequest.post("/auth/register", user);
  dispatch(registerSuccess(res.data));
} catch (err) {
  dispatch (registerFailed());
}
}

export const logout = async(dispatch, user) => {
  
  // Redirect back to login
  dispatch(loggingOut());
}
/* const handleClick = (e) => {
    e.preventDefault();
    register(dispatch, { firstName, lastName, email, username, password1 });
  };
*/
