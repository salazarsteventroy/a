import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: #b63958;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
`;

const Announcement = () => {
  return <Container> Evangelion Unit-02 (Rebuild)/New Unit-02 coming soon </Container>;
};

export default Announcement;