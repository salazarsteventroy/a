import styled from "styled-components";
import { mobile } from "../responsive";
import { UserContext } from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from "react-redux";
import { register } from "../redux/apiCalls";


const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background:
    url("https://c4.wallpaperflare.com/wallpaper/357/77/305/neon-genesis-evangelion-ayanami-rei-asuka-langley-soryu-ikari-shinji-wallpaper-preview.jpg")
      center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 37%;
  padding: 20px;
  background-color: #a5bdd6;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  height: 30%;
  width: 30%;
  border: none;
  padding: 10px 20px;
  background-color: #6e3e58;
  color: white;
  cursor: pointer;
`;

const Link2 = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

const Register = () => {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const dispatch = useDispatch();
  const [isActive, setIsActive] = useState(false);
   const { isFetching, error } = useSelector((state) => state.user);
   const password = password1;

   const handleClick = (e) => {
    e.preventDefault();
    register(dispatch, { firstName, lastName, email, username, password1 });
  };


useEffect(() => {
    // Validation to enable the submit buttion when all fields are populated and both passwords match
    if((firstName !== '' && lastName !== ''&& username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [firstName, lastName, username, email, password1, password2])

  return (

    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form>
          <Input
            placeholder="FirstName"
            onChange={(e) => setFirstName(e.target.value)}
          />
          <Input
            placeholder="LastName"
            type="password"
            onChange={(e) => setLastName(e.target.value)}
          />
          <Input
            placeholder="username"
            onChange={(e) => setUsername(e.target.value)}
          />
        
          <Input
            placeholder="email"
            onChange={(e) => setEmail(e.target.value)}
          />
          <Input type="password"
            placeholder="Password"
            onChange={(e) => setPassword1(e.target.value)}
          />
          <Input type="password"
            placeholder="Confirm Password"
            onChange={(e) => setPassword2(e.target.value)}
          />
          <Button onClick={handleClick} disabled={isFetching}>
            REGISTER
          </Button>
         <Link to="/login" >ALREADY HAVE AN ACCOUNT? </Link>
        </Form>
      </Wrapper>
    </Container>
  );
};


/*
   <Container>
      <Wrapper>
        <Title>CREATE AN ACCOUNT</Title>
        <Form>
          <Input placeholder="first name" />
          <Input placeholder="last name" />
          <Input placeholder="username" />
          <Input placeholder="email" />
          <Input type="password" placeholder="password" />
          <Input type="password" placeholder="confirm password" />
          <Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>PRIVACY POLICY</b>
          </Agreement>
          <Button>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>

*/

/*


const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const { isFetching, error } = useSelector((state) => state.user);
  const [isActive, setIsActive] = useState(true);


  const handleClick = (e) => {
    e.preventDefault();
    login(dispatch, { username, password });
  };


  useEffect(() => {

        if(username !== '' && password !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [username, password])


  return (

    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form>
          <Input
            placeholder="username"
            onChange={(e) => setUsername(e.target.value)}
          />
          <Input
            placeholder="password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          {isActive ?
          <Button onClick={handleClick} disabled={isFetching}>
            LOGIN
          </Button>

          :
          <Button onClick={handleClick}> Please fill the fields above </Button>
          }

          {error && <Error>Wrong username or password</Error>}

          <Link to="/register" exact>CREATE A NEW ACCOUNT </Link>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;
*/
export default Register;