import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import { logout } from "../redux/apiCalls";

export default function Logout(state){
	return(
		<Redirect to='/login' />
	)
}
